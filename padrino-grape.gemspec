Gem::Specification.new do |s|
  s.name        = 'padrino-grape'
  s.version     = '0.0.0'
  s.date        = '2014-03-27'
  s.summary     = 'Grape + Padrino playing nice together'
  s.description = 'Grape REST microframework patch to use with Padrino'
  s.authors     = ['AMar4enko']
  s.email       = 'amar4enko@gmail.com'
  s.files       = ['lib/padrino-grape.rb']
  s.homepage    = 'http://rubygems.org/gems/padrino-grape'
  s.license       = 'MIT'
  s.add_runtime_dependency 'padrino', '~> 0.12', '>= 0.12.0'
  s.add_runtime_dependency 'grape', '> 0.8.0', '>= 0.8.0'
  s.add_runtime_dependency 'activesupport', '> 3.0', '>= 3.0'
end
