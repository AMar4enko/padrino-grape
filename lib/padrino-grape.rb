Padrino::Reloader.class_eval do
  alias_method :_safe_load, :safe_load
  def safe_load(*args)
    _safe_load(*args) unless args.first[-3,3] == '.so'
  end
end

module Grape
  module Padrino
    extend ActiveSupport::Concern

    module ClassMethods
      def recursive_change!
        @endpoints.reject{|e| !e.options[:app] || !(e.options[:app] < Grape::API) }.each do |e|
          e.options[:app].recursive_change!
        end
        change!
      end
      def recursive_reset!
        @endpoints.reject{|e| !e.options[:app] || !(e.options[:app] < Grape::API) }.each do |e|
          e.options[:app].recursive_reset!
        end
        reset!
      end
      def reload?; true end
      def reload
        @d = dependencies
        recursive_reset!
        ::Padrino.require_dependencies(@d, force: true)
        ::Padrino.require_dependencies([app_file], force: true)
        recursive_change!
      end

      def prerequisites(&block)
        []
      end

      def dependencies(value = nil, &block)
        @dependencies = (instance_eval(&block) || []).map {|file| Dir[File.expand_path(File.join(File.dirname(app_file), file))] }.flatten.uniq if block_given?
        @dependencies = value.map {|file| Dir[File.expand_path(File.join(File.dirname(app_file), file))] }.flatten.uniq if value
        dependencies = (@dependencies ? @dependencies : [])
        (@endpoints.reject{|e| !e.options[:app].respond_to?(:dependencies)}.map do |endpoint|
          endpoint_app_file = endpoint.options[:app].app_file
          endpoint.options[:app].dependencies + (endpoint_app_file.start_with?(::Padrino.root) ? [endpoint_app_file] : [])
        end + dependencies).flatten.uniq.reverse
      end
      def public_folder;''end
      def setup_application!;end
      alias_method :reload!, :reload
    end
  end
end

if RACK_ENV == 'development'
  class Grape::API
    class << self
      alias_method :call_shadowed, :call
      def call(*args)
        if Time.now > (@last || 2.seconds.ago) + 1
          Thread.list.size > 1 ? Thread.exclusive { Padrino.reload! } : Padrino.reload!
          @last = Time.now
        end
        call_shadowed(*args)
      end
    end
  end
end

class Grape::API
  include Grape::Padrino

  class << self
    alias_method :inherited_shadowed, :inherited
    def inherited(subclass)
      Padrino::Reloader.exclude_constants << 'Grape' << 'Racc' << 'Rack'

      app_file =
          caller(1).map!{ |line| line.split(/:(?=\d|in )/, 3)[0,1] }.
              reject { |file, *_| Sinatra::Base.singleton_class::CALLERS_TO_IGNORE.any? { |pattern| file =~ pattern } }.
              flatten.first

      subclass.instance_eval <<-EVAL
          def app_file
            "#{app_file}"
          end
          def app_name
            "#{subclass.to_s}"
          end
      EVAL
      inherited_shadowed(subclass)
    end
  end

end
